Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
#get "/pages/*id" => 'pages#show', as: :page, format: false

 get 'calculator/clear' => 'calculator#clear'
  get 'calculator' => 'calculator#show'

  post 'calculator/:step' => 'calculator#create'
  get 'calculator/:step' => 'calculator#show'

get "/pages/*id" => 'pages#show', as: :page, format: false

root 'pages#show', id: 'index'


end
