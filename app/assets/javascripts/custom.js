/*
 Author: Pixar Themes.
 Author URI: http://www.pixarwpthemes.com/
 */

/*jslint browser: true*/
/*global $, jQuery, alert*/

jQuery(document).ready(function ($) {

    'use strict';

    function mycarousel_initCallback(carousel) {
        $('#mycarousel-next').bind('click', function () {
            carousel.next();
            return false;
        });
        $('#mycarousel-prev').bind('click', function () {
            carousel.prev();
            return false;
        });
    }

    $('#mycarousel').jcarousel({
        auto: 2,
        wrap: 'last',
        scroll: 1,
        initCallback: mycarousel_initCallback,
        buttonNextHTML: null,
        buttonPrevHTML: null
    });

    $('a.prettyPhoto').prettyPhoto({
        deeplinking: false,
        theme: 'light_square',
        social_tools: false
    });

    /*$('.top-menu .menu-item a').click(function (e) {
		e.preventDefault();
        var link = $(this).attr('href');
        $.scrollTo(link, 1000, {offset:-80});
    });

    $('#contactForm').validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                beforeSubmit:  function() {
                    $('#loading').css('visibility', 'visible');
                },
                success: function(responseText, statusText, xhr, $form) {
                    $('#loading').css('visibility', 'hidden');
                    $('#contactForm').trigger('reset');
                    $('#success').fadeIn().html(responseText);
                }
            });
            return false;
        }
    });*/

});