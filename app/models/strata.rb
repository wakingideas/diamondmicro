
class Strata < ActiveRecord::Base


  def self.v(carat,color,clarity,shape)
    where("min_carat <= ? and max_carat >= ? and color = ? and shape = ?",carat.to_f,carat.to_f,color,shape).where(:clarity => clarity).first.param.to_f
  end 
 
end
