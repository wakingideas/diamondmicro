class WizardState 
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend  ActiveModel::Naming 

  def initialize(session_params={})
    @defaults = HashWithIndifferentAccess.new(
      :shape => "round",
      :carat => 0.24,
      :hascertificate => "YES",
      :certificate => "GIA",
      :color => "color.f1",
      :cut => "cut.f1",
      :clarity => "clar.f1",
      :polish => "pol.f1",
      :culet => "cul.f1",
      :sym => "sym.f1",
      :fluor => "fluor.f1",
      :depth => "61.0",
      :table => "57",
      :wlratio => "1.00",
      :max_girdle => "Medium",
      :min_girdle => "Medium",
      :value_type => "store"
    )

    session_params = @defaults.merge(session_params)
    
    Rails.logger.info "--- init ---"
    session_params.each do |p,v|
      Rails.logger.info "#{p.to_s}= #{v}"
      send("#{p.to_s}=",v)
    end
     
    Rails.logger.info "--- init over ---"
 
  end 

  def defaults
    @defaults
  end 

  def to_hash
    {
      :shape => @shape,
      :carat => @carat,
      :certificate => @certificate,
      :hascertificate => @hascertificate,
      :clarity => @clarity,
      :color => @color,
      :wlratio => @wlratio,
      :fluor => @fluor,
      :culet => @culet,
      :min_girdle => @min_girdle,
      :max_girdle => @max_girdle,
      :depth => @depth,
      :table => @table,
      :sym => @sym,
      :polish => @polish,
      :min_girdle => @min_girdle,
      :max_girdle => @max_girdle,
      :value_type => @value_type
    }
  end 


  def default_for(sym)
    @defaults.fetch(sym,nil)
  end 

  def update(params)
    # Rails.logger.info "UPDATE" 
    # Rails.logger.info "------ Defaults ------"
    # Rails.logger.info @default.inspect 
    # Rails.logger.info "------ Params ---------"
    # Rails.logger.info params.inspect
 
    params = @defaults.merge(params) unless params.nil?

    # Rails.logger.info "------ Params merged---------"
    # Rails.logger.info params.inspect
 


    params.each do |p,v|
    #  Rails.logger.info "#{p.to_s}= #{v}"
      send("#{p.to_s}=",v)
    end
    
  
  end 

  attr_accessor :shape
  attr_accessor :carat
  attr_accessor :certificate, :hascertificate
  attr_accessor :color
  attr_accessor :clarity
  attr_accessor :cut
  attr_accessor :color, :wlratio, :fluor, :culet
  attr_accessor :girdle, :depth, :table, :sym, :polish
  attr_accessor :value_type
  attr_accessor :min_girdle
  attr_accessor :max_girdle

  def girdles 
    GIRDLES
  end 

  def current_step=(step)
    if step.nil?
      @current_step = steps.first
    else
      @current_step = step.to_sym
    end 
  end 
  
  def save
    true
  end


  def current_step
      step = @current_step || steps.first
      step.to_sym
  end

  def steps
    [:shape, :carat, :cert, :color ,:clarity, :cut , :value_type, :results]
  end

  def next_step
      self.current_step = steps[steps.index(current_step)+1]
  end

  def next_step_path
    "/calculator/#{next_step.to_s}"
  end 


  def previous_step
      self.current_step = steps[steps.index(current_step)-1]
  end

  def first_step?
      current_step == steps.first
  end

  def last_step?
      current_step == steps.last
  end
  def current_template
    "#{current_step.to_s}.html.erb"
  end 

  def wizard_state_path
    "/show"
  end 

  def label_lookup(k)
    LABELS.fetch(k,nil)
  end 
  LABELS = {
  
    "fluor.f1" => "None",
    "fluor.f2" => "Faint",
    "fluor.f3" => "Medium",
    "fluor.f4" => "Strong",
    "fluor.f5" => "Very Strong",
    "cul.f1" => "None",
    "cul.f2" =>"Very Small",
    "cul.f3" => "Small",
    "cul.f4" => "Medium",
    "cul.f5" => "Slightly Large",
    "sym.f1" => "Ideal/Excellent",
    "sym.f2" => "Very Good",
    "sym.f3" => "Good",
    "sym.f4" => "Fair",
    "pol.f1" => "Ideal/Excellent",
    "pol.f2" => "Very Good",
    "pol.f3" => "Good",
    "pol.f4" => "Fair",
    "cut.f1" => "Ideal/Excellent",
    "cut.f2" => "Very Good",
    "cut.f3" => "Good",
    "cut.f4" => "Fair",
    "cut.f5" => "Poor",
    "color.f1" => "D",
    "color.f2" => "E",
    "color.f3" => "F",
    "color.f4" => "G",
    "color.f5" => "H",
    "color.f6" => "I",
    "color.f7" => "J",
    "clar.f1" => "IF/F",
    "clar.f2" => "VVS1",
    "clar.f3"  => "VVS2",
    "clar.f4" => "VS1",
    "clar.f5" => "VS2",
    "clar.f6" => "SI1",
    "clar.f7" => "SI2"


 }   
 
 GIRDLES = [ "Extremely Thin", "Very Thin", "Thin", "Medium", "Slightly Thick", "Thick", "Very Thick", "Extremely Thick" ]
  

end 
