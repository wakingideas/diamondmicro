class ValueTypeMarkup < ActiveRecord::Base

  def self.markup_by_type(t)
    where("value_type = ?",t).first.markup
  end 

end
