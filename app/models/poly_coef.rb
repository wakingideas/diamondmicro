class PolyCoef < ActiveRecord::Base

  def self.value_by_shape(shape,carat,wlratio)

   # sql = "SELECT intercept + degree1 * Log(?) + degree2 * log(?)^2 + degree3 * log(?)^3 + degree4 * log(?)^4 + degree5 * log(?)^5 + jitter * log(?) FROM PolyCoefs WHERE Shape = ?"
    
    res = where(:shape => shape).first
    
    if res.nil?
      Rails.logger.info "--- red nil ---"
      Rails.logger.info res.inspect
      Rails.logger.info shape
      Rails.logger.info carat
      Rails.logger.info wlratio
    end 

    i  = Float(res.intercept)
    carat = Float(carat)
    lcarat = Math.log(carat)
    d1 = Float(res.degree1) * lcarat  
    d2 = Float(res.degree2) * (lcarat ** 2) 
    d3 = Float(res.degree3) * (lcarat ** 3)
    d4 = Float(res.degree4) * (lcarat ** 4)
    d5 = Float(res.degree5) * (lcarat ** 5)
    j = Float(res.jitter)  * Math.log(wlratio) 
   
    
    
    puts i
    puts d1 
    puts d2 
    puts d3 
    puts d4 
    puts d5 
    puts j

    r = i + d1 + d2 + d3 + d4 + d5 + j 
    r
  end 

end
