class GirdleRange < ActiveRecord::Base
  
  def self.find_by_min_max(min,max)
    results = where("min_girdle = ? and max_girdle = ?", min, max)
    if results.any?
      results.first.function_name.chomp
    else
      "girdle.f1"
    end 
  
  end 

end
