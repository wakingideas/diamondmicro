class RawValue < ActiveRecord::Base
	def maxdiffupdate

	if self.clarity == "IF"
		clarity = "Clar.f1"
	elsif self.clarity == "VVS1"
		clarity = "Clar.f2"
	elsif self.clarity == "VVS2"
		clarity = "Clar.f3"
	elsif self.clarity == "VS1"
		clarity = "Clar.f4"
	elsif self.clarity == "VS2"	
		clarity = "Clar.f5"
	elsif self.clarity == "SI1"
		clarity = "Clar.f6"
	elsif self.clarity == "SI2"
		clarity = "Clar.f7"
	end

	if self.color == "D"
		color = "Color.f1"
	elsif self.color == "E"
		color = "Color.f2"
	elsif self.color == "F"
		color = "Color.f3"
	elsif self.color == "G"
		color = "Color.f4"
	elsif self.color == "H"
		color = "Color.f5"
	elsif self.color == "I"
		color = "Color.f6"
	elsif self.color == "J"
		color = "Color.f7"
	end

	max_value = PriceQuoteCalculator.new(WizardState.new(:carat => self.carat, :depth => self.depth, :table => self.table, :clarity => clarity, :color => color,:value_type => "Internet").to_hash).price1
  	
  	cdiff = (max_value.to_f - self.price.to_f) / (self.price.to_f)
  	self.update_attributes(:max_value => max_value, :difference => cdiff)

  	if (self.difference * 100).abs >= 35
  		self.update_attributes(:outlier => true)
  	end
  	if (self.difference  * 100) >= 15
  	 self.update_attributes(:deal => true)
  	end	
	end
 


end
