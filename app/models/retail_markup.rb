class RetailMarkup < ActiveRecord::Base

  def self.markup_from_wholesale_price(price)
    where("min_wholesale_price <= ? and max_wholesale_price > ?",price,price).first.round_markup
  end 

end
