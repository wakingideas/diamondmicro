class StrataMarkup < ActiveRecord::Base

  def self.markup_from_carat(carat)
    where("min_carat <= ? AND ? <= max_carat",carat,carat).first.markup
  end 

end
