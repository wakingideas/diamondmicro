class PriceQuoteCalculator 

  def initialize(params)
    @params = params
   
   #  puts "-----params--------"
   #  puts params.inspect 
   #  # girdle range 
    @min_girdle = params.fetch(:min_girdle,"Medium")
    @max_girdle = params.fetch(:max_girdle,"Medium")
    @girdle_function = GirdleRange.find_by_min_max(@min_girdle,@max_girdle)
    # puts "--- girdle ---"
    # puts @girdle_function

    # depth function
    @depth = params.fetch(:depth,61.0)
    @depth_function = get_depthfunction   
    #table function 
    @table = params.fetch(:table,57)
    @table_function = get_tablefunction  
  #  puts "============="
    # get the markup 
    @value_type = params.fetch(:value_type,"store")
    @carat      = params.fetch(:carat,0.23)
    @color      = params.fetch(:color,"color.f1")
    # width ratio
    @wlratio    = params.fetch(:wlratio,1)
    @clarity    = params.fetch(:clarity,"clar.f1")
    @shape      = params.fetch(:shape,'round') 
    @cul        = params.fetch(:cul,"cul.f1")
    @cut        = params.fetch(:cut,"cut.f1")
    @polish     = params.fetch(:polish,"pol.f1")
    @symmetry   = params.fetch(:symmetry,"sym.f1")
    @fluor      = params.fetch(:fluor,"fluor.f1")
 
    sp
  
    #puts "Shape param : #{@shapeparam}"
    generate_sql
  end 

  def is_princess?
    @shape.downcase == "princess"
  end 

  def is_round? 
    @shape.downcase == "round"
  end 

  def is_other?
    s = @shape.downcase
    s != "round" && s != "princess" 
  end 

  def sp
 
    @shapeparam = @shape
    if ( @shapeparam.downcase != "round" && @shapeparam.downcase != "princess" )
      @shapeparam = 'other'
    end 
 
    puts "Shape param : #{@shapeparam}"
  end

  
  def generate_sql
    sql = ""
  	sql = sql + "SELECT InternetValue + InternetValue * " 
		sql = sql + "( SELECT markup FROM value_type_markups WHERE value_type = '#{@value_type}') as DiamondPrice"
	  sql = sql + " FROM ("
		sql = sql + " SELECT round( retailvalue * " 
    sql = sql + "( SELECT 1 + markup FROM max_markups  WHERE min_dollar <= retailvalue AND retailvalue < max_dollar ) "
		sql = sql + ") as internetvalue FROM ( SELECT ( "
    
    # internet value 
    sql = sql + " SELECT wholesaleprice * "
    

    if @shapeparam.downcase == "round"
      markupcolumn = "round_markup"
    elsif @shapeparam.downcase == "princess" 
      markupcolumn = "princess_markup"
    else
      markupcolumn =  "fancy_markup"
    end 

    sql = sql + "(1 + #{markupcolumn})"

    if @shapeparam.downcase == "other"
      sql =  sql + " * (1 - " 
      s = @shape.downcase
      if  (s == "emerald" && @wlratio.to_f >= 1.06 && @wlratio <= 1.29 )
        n =  "0.02"
      elsif s == "emerald" && @wlratio.to_f >= 1.50
        n = "0.02"
      elsif s == "radiant" && @wlratio.to_f >= 1.06 && @wlratio <= 1.19
        n = "0.03"							
      elsif s == "heart" && @wlratio.to_f >= 1
        n = "0.33*log(#{@wlratio})"
      elsif s == "heart" && @wlratio.to_f <= 1
        n =   "-0.3*log(#{@wlratio})"
      elsif s == "pear" && @wlratio.to_f >= 1.50
        n =   "0.1*log(#{@wlratio}-0.5)"
      elsif s == "pear" && @wlratio.to_f <= 1.50
        n =  "-0.1*log(#{@wlratio}-0.5)"
      else
        n = "0"
      end
		  sql = sql + " #{n} )"
    elsif @shapeparam.downcase == "round" 
					sql = sql +	"* (1 + ( SELECT markup FROM strata_markups  WHERE min_carat <= #{@carat} AND #{@carat} <= max_carat ) ) "
    end 
 		
      sql = sql + " FROM retail_markups "
      sql = sql + " WHERE min_wholesale_price <= wholesaleprice AND wholesaleprice < max_wholesale_price ) as retailvalue "

      sql = sql + "FROM ( "
   
      # wholesale pricing 

      wsql = ""
      wsql = wsql + "( SELECT intercept + "
      wsql = wsql + "CAST( degree1 * Log(#{@carat}) as DECIMAL(12,2) ) + "
      wsql = wsql + "CAST( degree2 * POWER(log(#{@carat}),2) as DECIMAL(12,2) ) + " 
      wsql = wsql + "CAST( degree3 * POWER(log(#{@carat}),3) as DECIMAL(12,2) ) + "
      wsql = wsql + "CAST( degree4 * POWER(log(#{@carat}),4) as DECIMAL(12,2) ) + "
			wsql = wsql + "CAST( degree5 * POWER(log(#{@carat}),5) as DECIMAL(12,2) ) + "
      wsql = wsql + "CAST( jitter * log(#{@wlratio}) as DECIMAL(12,2) ) "
			wsql = wsql + "FROM poly_coefs WHERE shape = '#{@shapeparam}' "
			wsql = wsql + ") + ("
	
      wsql = wsql + " SELECT sum(param) FROM properties WHERE Property in "
      wsql = wsql + "('#{@cut}', '#{@depth_function}', '#{@table_function}', '#{@girdle_function}', '#{@cul}', '#{@polish}', '#{@symmetry}' "
      wsql = wsql + ", '#{@fluor}'" if @shapeparam.downcase != "princess"
      wsql = wsql + ") AND shape = '#{@shapeparam}' ) + "
	
      if @shapeparam.downcase == "princess"
					wsql = wsql + "( SELECT param FROM princess_color_fluors WHERE color = '#{@color}' AND fluor = '#{@fluor}' ) + "
      end 
	    
      if @shapeparam.downcase == "other"
          wsql = wsql + " ( SELECT param * #{@depth} FROM properties WHERE property = 'Depth' AND shape = '#{@shapeparam}') + "
          wsql = wsql + " ( SELECT	param * #{@table} FROM properties WHERE property = 'Table' AND shape = '#{@shapeparam}') +	"
          wsql = wsql + " ( SELECT	param * #{@wlratio} FROM properties WHERE property = 'Ratio.#{@shape}' AND shape = '#{@shapeparam}') + "
					wsql = wsql + " ( SELECT	param FROM properties WHERE property = 'Shape.#{@shape}' AND shape = '#{@shapeparam}') + "
      end

			wsql = wsql + " ( "
			wsql = wsql + "SELECT param FROM strata WHERE min_carat <= #{@carat} AND #{@carat} <= max_carat "
			wsql = wsql + "AND color = '#{@color}' AND clarity = '#{@clarity}' AND shape = '#{@shapeparam}' "
			wsql = wsql + " )" 

      # todo change to POWER
      if @shapeparam.downcase == "other"
        sql = sql + "SELECT POWER(((("
        sql = sql + " " + wsql + " "
        sql = sql + " )*0.06060606)+1),(1/0.06060606)) " 
        sql = sql + " as wholesaleprice ) as WholesaleView ) RetailView ) InternetView "
      else

        sql = sql + "SELECT exp("
        sql = sql + " " + wsql + " "
        sql = sql + " )" 
        sql = sql + " as wholesaleprice ) as WholesaleView ) RetailView ) InternetView "
      end
      
      sql
  end 


  def price1


  
    p =  ActiveRecord::Base.connection.execute(generate_sql)
    puts "#{p.first}"
    price = p.first.first

   
    if @certificate != "GIA"
      if @certificate == "AGS"
        price = price * 1.08
      else 
        price = price * 0.90
      end 
    end    
   
    begin
      return price.round 
    rescue
      return price
    end 
  end 

  def price
    return 0 if @params[:carat] == 0
   
    @wholesaleprice = get_wholesaleprice
    @round_markup   = RetailMarkup.markup_from_wholesale_price(@wholesaleprice) 
    @strata_markup  = StrataMarkup.markup_from_carat(@carat) 
    @retailvalue    = @wholesaleprice * (1 + @round_markup) * (1 + @strata_markup)
    @max_markup     = MaxMarkup.multiplier_from_retailvalue(@retailvalue)
    @internetvalue  = @retailvalue * @max_markup  
    @markup_by_type = ValueTypeMarkup.markup_by_type(@value_type)
    @diamond_price  = @internetvalue + (@internetvalue * @markup_by_type)

    return @diamond_price
  end 

  def wholesaleprice
     
      p = poly_coef + strata + props
      puts p 
      Math.exp(p)
  end

  def poly_coef
      puts "poly coef"
      puts "shape: #{@shape}"
      puts "carat: #{@carat}"
      puts "carat: #{@wlratio}"
      PolyCoef.value_by_shape(@shape,@carat,@wlratio)
  end 

  def strata
      puts "Strata"
      r = Strata.v(@carat,@color,@clarity,@shape) 
      puts r
      r
  end 
  
  def props
      puts "=========================="
      puts "=========================="
      puts "cut : #{@cut}"
      puts "depthfunction : #{@depth_function}"
      puts "tablefunction : #{@table_function}"
      puts "girdle function : #{@girdle_function}"
      puts "cul : #{@cul}"
      puts "fluor : #{@fluor}"
      puts "polish: #{@polish}"
      puts "shape: #{@shape}"
      puts "symmetry: #{@symmetry}"

      puts "=========================="
      Rails.logger.info @depth
      Rails.logger.info  @tablefunction

      Property.v(@cut,@depth_function,@table_function,@girdle_function,@cul,@polish,@symmetry,@fluor,@shape)    
  end 


  def self.get_shape(shape)
    Rails.logger.info "Getting shape info for *#{shape}*"
    d = Shape.where("lower(shape) = ?",shape.downcase.strip)
    d.first
  end 



private 
 

  def get_wholesaleprice 
      puts "hello"
      puts a
      puts b
      puts c
      price = a.to_f + b.to_f + c.to_f  
      Rails.logger.info price
      Math.exp(price)
  end 

 
  def get_depthfunction
    d = @depth.to_f
    if    ( d >= 60.1 && d <= 62.8) 
      return "depth.f1"
    elsif ( d >= 58.5 && d <= 64) 
      return "depth.f2"
    elsif ( d >= 57.5 && d <= 64.5) 
      return "depth.f3"
    elsif ( d >= 56.5 && d <= 67) 
      return "depth.f4"
    elsif ( d >= 0  && d <= 100) 
      return "depth.f5"
    end 
  end 

   
  def get_tablefunction
    t = @table.to_f
    if    ( t >= 53 && t <= 57) 
      return "table.f1"
    elsif ( t >= 52 && t <= 60) 
      return "table.f2"
    elsif ( t >= 51 && t <= 65) 
      return "table.f3"
    elsif  ( t >= 50 && t <= 67) 
      return "table.f4"
    elsif ( t >= 0  && t <= 100) 
      return "table.f5"
    end 
  end 


  def get_cert_multiplier
      if (@cert != "GIA")
        if (@cert == "AGS")
          1.08
        else
          0.90
        end      
      else
        1
      end 
  end 

end 
