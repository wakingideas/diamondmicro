class MaxMarkup < ActiveRecord::Base


  def self.multiplier_from_retailvalue(p)
    markup = where("min_dollar <= ? AND ? < max_dollar",p,p).first.markup
    markup + 1
  end 

end
