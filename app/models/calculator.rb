class Calculator

 
  
  def test
    diverance("other", 34, "table.f1", 1,"Round",0.23,"cut.f1","cul.f1","pol.f1","sym.f1","color.f1","fluor.f1","depth.f1","table.f1","girdle.f1","clar.f1","Internet")
  end 




  def diverance(shapeparam,depth,table,wlratio,shape,carat,cut,cul,pol,sym,color,fluor,depthfunction,tablefunction,girdlefunction,clarity,value_type)
   
    sql = ""
  	sql = sql + "SELECT InternetValue + InternetValue * " 
		sql = sql + "( SELECT markup FROM value_type_markups WHERE value_type = '#{value_type}') as DiamondPrice"
	  sql = sql + " FROM ("
		sql = sql + " SELECT round( retailvalue * " 
    sql = sql + "( SELECT 1 + markup FROM max_markups  WHERE min_dollar <= retailvalue AND retailvalue < max_dollar ) "
		sql = sql + ") as internetvalue FROM ( SELECT ( "
	  
    sql = sql + " SELECT wholesaleprice * "
    

    if shapeparam == "Round"
      markupcolumn = "round_markup"
    elsif shapeparam == "Princess" 
      markupcolumn = "princess_markup"
    else
      markupcolumn =  "fancy_markup"
    end 

    sql = sql + "(1 + #{markupcolumn})"

    if shapeparam == "other"
      sql =  sql + " * (1 - " 
      if  shape ==  "emerald" and ratio gte 1.06 and ratio lte 1.29
        n =  "0.02"
      elsif shape == "emerald" and ratio gte 1.50
        n = "0.02"
      elsif shape == "radiant" and ratio gte 1.06 and ratio lte 1.19>
        n = "0.03"							
      elsif shape == "heart" and ratio gte 1>
        n = "0.33*log(#{ratio})"
      elsif shape == "heart" and ratio lte 1>
        n =   "-0.3*log(#{ratio})"
      elsif shape == "pear" and ratio gte 1.50>
        n =   "0.1*log(#{ratio}-0.5)"
      elsif shape == "pear" and ratio lte 1.50>
        n =  "-0.1*log(#{ratio}-0.5)"
      else
        n = "0"
      end
		  sql = sql + " #{n} )"
    elsif shapeparam == "Round" 
					sql = sql +	"* (1 + ( SELECT markup FROM strata_markups  WHERE min_carat <= #{carat} AND #{carat} <= max_carat ) ) "
    end 
 		
      sql = sql + " FROM retail_markups WHERE min_wholesale_price <= wholesaleprice AND wholesaleprice < max_wholesale_price ) as retailvalue FROM ( "
     
      if shapeparam == "other">
        sql = sql + "SELECT ((("
      else 
        sql = sql + "SELECT exp("
      end 
      
      sql = sql + "( SELECT intercept + degree1 * Log(#{carat}) + degree2 * log(#{carat})^2 " 
      sql = sql + "+ degree3 * log(#{carat})^3 + degree4 * log(#{carat})^4 + "
			sql = sql + "degree5 * log(#{carat})^5 + jitter * log(#{wlratio}) "
			sql = sql + "FROM poly_coefs WHERE shape = '#{shapeparam}' "
			sql = sql + ") + ("
	
      sql = sql + " SELECT sum(param) FROM properties WHERE Property in "
      sql = sql + "('#{cut}', '#{depthfunction}', '#{tablefunction}', '#{girdlefunction}', '#{cul}', '#{pol}', '#{sym}' "
      sql = sql + ", '#{fluor}'" if shapeparam != "Princess"
      sql = sql + ") AND shape = '#{shapeparam}' ) + "
	
      if shapeparam == "Princess"
					sql = sql + "( SELECT param FROM princess_color_fluors WHERE color = '#{color}' AND fluor = '#{fluor}' ) + "
      end 
	
      if shapeparam == "other">
					
          sql = sql + " ( SELECT param * #{depth} FROM properties WHERE property = 'Depth' AND shape = '#{shapeparam}') + "
          sql = sql + " ( SELECT	param * #{table} FROM properties WHERE property = 'Table' AND shape = '#{shapeparam}') +	"
          sql = sql + " ( SELECT	param * #{wlratio} FROM properties WHERE property = '#{shape}' AND shape = '#{shapeparam}') + "
					sql = sql + " ( SELECT	param FROM properties WHERE property = '#{shape}' AND shape = '#{shapeparam}') + "
      end

			sql = sql + " ( "
			sql = sql + "SELECT param FROM strata WHERE min_carat <= #{carat} AND #{carat} <= max_carat "
			sql = sql + "AND color = '#{color}' AND clarity = '#{clarity}' AND shape = '#{shapeparam}' "
			sql = sql + " )" 
	
      if shapeparam == "other">
        sql = sql + " )*0.06060606)+1)^(1/0.06060606) " 
      else
        sql = sql + " )" 
      end
      
      sql = sql + " as wholesaleprice FROM Unity ) as WholesaleView "
  	  sql = sql + " ) RetailView ) InternetView"
      sql
  end 

end 
