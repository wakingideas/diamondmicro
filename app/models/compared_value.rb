class ComparedValue < ActiveRecord::Base
	def maxdiffupdate

	max_value = PriceQuoteCalculator.new(WizardState.new(:carat => self.carat, :clarity => self.clarity, :color => self.color,:value_type => "Internet").to_hash).price1
  	
  	cdiff = (max_value.to_f - self.vendor_price.to_f) / (self.vendor_price.to_f)
  	self.update_attributes(:max_value => max_value, :difference => cdiff)
	end
end
