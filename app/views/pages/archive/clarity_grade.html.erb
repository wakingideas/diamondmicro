<section id="feature" class="features" style='margin-top:35px;'>
<h2>Clarity Grade</h2>
<br>

<p>Gem-quality diamonds are evaluated and graded by their clarity, the absence of naturally occurring internal flaws, called Inclusions, and external flaws, called Blemishes.  Inclusions and Blemishes, whether visible to the naked eye or only under a 10X microscope, can interfere with a diamond's ability to optimally reflect light, thus reducing the diamond's brilliance, fire and scintillation.  More importantly, flaws impact beauty and desirability; there is much greater demand for a diamond with no flaws or very few flaws. </p>

<p>A diamond's flaws give it a unique fingerprint, as no two diamonds will have exactly the same flaws. A grading report from a geological lab that grades diamonds will usually include not only a Clarity Grade, but also a diagram indicating the type and location of any flaws. </p>

<p>Clarity Grade is a strong factor in the value of a diamond. The better the Clarity Grade, the more valuable the diamond is. This is because there is greater market demand for diamonds without significant Inclusions and Blemishes.  The better the Clarity Grade, the greater the demand is for the diamond.  At the same time, diamonds without Inclusions, referred to as Internally Flawless, are more rare. The result is that the value of diamonds exponentially increases as Clarity Grade increases. </p>

<p>Most Inclusions will not substantially affect a diamond's performance or structural integrity, unless the Inclusion is a large cloud that interferes with the transmission of light, or a large crack close to or breaking the surface, which can make the diamond susceptible to fracture.</p>

<p>Clarity Grades are determined by gemological labs that certify diamonds. Different labs use different grading scales.  The lab which grades the vast majority of retail diamonds is the Gemological Institute of America (GIA). The GIA uses the following scale:</p>
<br>

<%= image_tag "Archive_Clarity_scale.jpg" , alt:"Diamond Clarity Scale" %>

<br>
<p><b><br>GIA CLARITY GRADE SCALE</b> <br><br>
<b>F  - Flawless.</b> No Inclusions or Blemishes observable under 10X magnification when evaluated by an experienced grader. </p>
	
<p><b>IF  - Internally Flawless.</b> The diamond has no inclusions when examined by an experienced grader using 10X magnification, but has minor blemishes. </p>

<p><b>VVS1 / VVS2 - Very, Very Slightly Included.</b> Contains minute inclusions observable only under 10X magnification by experienced graders. </p>
	
<p><b>VS1 / VS2 - Very Slightly Included.</b> Contains minute inclusions (e.g., crystals, clouds or feathers) when observed under 10X magnification. </p>

<p><b>SI1 / SI2 - Slightly included. </b>Inclusions are seen under 10x magnification or may be visible to the naked eye. </p>

<p><b>I1 / I2 / I3  - Included.</b> Inclusions are obvious under 10x magnification and visible to the unaided eye. </p>
<br><br>
<p><b>Clarity Flaws</b> <br><br>
Flaws impacting diamond clarity are of two types: Inclusions created naturally during the geological formation of the diamond, and Blemishes either created during geologic formation or during mining or fabrication processes. Inclusions are flaws on the inside of a diamond; Blemishes are flaws on the surface of a diamond. </p>

<p>Many different types of clarity flaws can be observed in diamonds.  Some of the common ones are: </p>

<p><b>Pinpoints:</b>   A very small white dot on the surface of the stone.  By far, the most common flaw </p>

<p><b>Carbons:</b>   A very small black dot on or within the stone.  </p>

<p><b>Feathers:</b>   Small cracks similar in appearance to a small white feather.  </p>

<p><b>Clouds:</b>   Hazy areas made up of many small crystals that cannot be observed individually.</p>

<p><b>Crystal Growths:</b>  small crystals formed within the diamond.</p>

<p>The grading of clarity is done by trained lab technicians using 10X power microscopes. The factors considered in grading include: size, number, position, nature, and color or relief:</p>
 
<p><b><br>Size: Magnitude of Individual Flaws</b><Br><br> 
The larger the flaw, the greater it will impact the Clarity Grade. This is especially true as the flaw becomes observable to the naked eye.  </p>

<p><b><br>Number: Quantity of Flaws </b><Br><br> 
The Clarity Grade is also impacted by the number of flaws observed in the diamond. </p>

<p><b><br>Position: Location of Flaws</b><Br><br> 
Where the flaw is can influence the degree to which the flaw negatively impacts the Clarity Grade of the diamond. If the flaws are Inclusions directly under the Table and observable with the unaided eye, these will have a greater negative influence.  </p>

<p><b><br>Nature: Type of flaws</b><Br><br> 
Certain flaws are regarded as more serious than others. Cracks that raise questions regarding the integrity of the structure of the diamond will have significant negative influence on the diamond's Clarity Grade. Large Clouds and Crystal Growths that seriously interfere with the performance of the diamond will have similar negative impacts on the Clarity Grade. </p>


<p><b><br>Color or Relief: Appearance</b><Br><br> 
The color and contrast of a flaw will also influence the degree to which the flaw will negatively impact the Clarity Grade.</p>
 <br>
<p><b>Grading Standards </b><Br><br> 
 
While there is not a single standard used by all the various geological labs for grading the <b>Clarity of diamonds</b>, the labs' practices are quite similar.  Clarity Grades are assessed based on the most noticeable or significant inclusions, called "grade setting" Inclusions. Other less significant inclusions may be indicated on grading charts, but do not establish the Clarity Grade of the diamond.  Clarity grading is done on loose diamonds (un-mounted) viewed from all angles in appropriate lighting using a 10X microscope or jeweler's loupe. </p>

<p>Some labs use different grading scales than the one used by GIA, the leading grader of diamonds, but the scales are basically the same. </p>

<p>One of the other respected labs, the American Gem Society (AGS) uses a number scale from 0 and 10. The numbers correlate closely with the GIA grading system.</p>

<p>The European Gemological Laboratory (EGL-USA) uses the same grading scale as the GIA, except EGL-USA has added an SI3 clarity grade.</p>



<p><b><br>Clarity and Rarity </b><Br><br> 
 
Flawless does not mean there are no flaws, it only means that it takes magnification greater than 10X to see them. Even still, diamonds that have highest clarity grade are not common. Only about 6% of retail gem-quality diamonds bear a Flawless (FL) or Internally Flawless (IF) Clarity Grade.  </p>

<p><b><br>Clarity Enhancement</b><Br><br> 
 
There are several artificial techniques used to try to improve the Clarity Grade of a diamond, the most common of which is Laser Drilling. This involves using a laser to make a hole to remove an Inclusion. The hole is often then filled. Laser Drilling is detectable by experienced diamond graders and negatively impacts the Clarity Grade of the diamond. </p>

<p><b><br>History of Clarity Grading</b><Br><br> 
The GIA initiated clarity grading in 1953, with system for grading the clarity of diamonds and a grading scale that included nine different grades: Flawless, VVS1, VVS2, VS1, VS2, SI1, SI2, I1, and I2. The "I" at that time stood for Imperfect, but this was changed in the 1990's to mean Included. The Internally Flawless grade and the I3 grade were added in the 1970's.</p>
</section>   

