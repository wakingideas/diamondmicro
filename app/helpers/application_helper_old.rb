module ApplicationHelper
  def title(*parts)
    unless parts.empty?
      content_for :title do
        (parts << "Ticketee").join(" - ")
      end
    end 
  end

  def keywords(s)
    content_for :keywords do
      s
    end 
  end 

  def description(s)
    content_for :description do
      s
    end 
  end 



end
