class CalculatorController < ApplicationController
 
 layout "calculator"

  def clear
  
    session[:wizard_params] = {}
    session[:wizard_step] = nil
    render :text => "cleared"
  end 

  def show
    
    session[:wizard_params] ||= {}
    @wizard_state = WizardState.new(session[:wizard_params])
    render @wizard_state.current_template
  end

   def update
    session[:wizard_params] ||= {}
    @wizard_state = WizardState.new(session[:wizard_params])
    render @wizard_state.current_template
   end 

   def create 

    # Rails.logger.info " ----- step -----"
    # Rails.logger.info params[:step]
    session[:wizard_params].deep_merge!(params[:wizard_state]) if params[:wizard_state]
    @wizard_state = WizardState.new(session[:wizard_params])
    @wizard_state.current_step = params[:step]
    
    # Rails.logger.info "----- create -----"
    # Rails.logger.info session[:wizard_params].inspect 
    # Rails.logger.info session[:wizard_step].inspect 
    
    # if params[:back_button]
    #   @wizard_state.previous_step
    # elsif @wizard_state.last_step?
      
    # else
    #   @wizard_state.next_step
    # end
    render @wizard_state.current_template
   end 

end
