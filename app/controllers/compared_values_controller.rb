class ComparedValuesController < ApplicationController
   before_filter :authenticate_admin_user!
  def add_value
  	max_value = PriceQuoteCalculator.new(WizardState.new(:carat => params[:compared_value][:carat], :clarity => params[:compared_value][:clarity], :color => params[:compared_value][:color],:value_type => "Internet").to_hash).price1
  	
  	cdiff = (max_value.to_f - params[:compared_value][:vendor_price].to_f) / (params[:compared_value][:vendor_price].to_f)
  	cv = ComparedValue.new(:carat => params[:compared_value][:carat], :clarity => params[:compared_value][:clarity], :color => params[:compared_value][:color], :cut_grade => params[:compared_value][:cut_grade], :vendor_name => params[:compared_value][:vendor_name], :vendor_price => params[:compared_value][:vendor_price], :max_value => max_value, :difference => cdiff, :retail_type => "Internet", :cert_type => "GIA", :shape => "Round")
  	cv.save 
  	redirect_to '/compared_values'
  end 

 def index 
 	@raw_values = RawValue.where(:median => true).all
  	@colors = ["D","E","F","G","H","I","J"]
	@clarity = ["IF","VVS1","VVS2","VS1","VS2","SI1","SI2"]

   @roundstratas = [(0.20..0.29),
        (0.30..0.39),
        (0.40..0.49),
        (0.50..0.59),
        (0.60..0.69),
        (0.70..0.74),
        (0.75..0.79),
		(0.80..0.89),
		(0.90..0.99),
		(1.00..1.09),
		(1.10..1.19),
		(1.20..1.24),
		(1.25..1.29),
		(1.30..1.39),
		(1.40..1.49),
		(1.50..1.59),
		(1.60..1.69),
		(1.70..1.74),
		(1.75..1.89),
		(1.90..1.99),
		(2.00..2.10),
		(2.11..2.24),
		(2.25..2.49),
		(2.50..2.74),
		(2.75..2.89),
		(2.90..2.99),
		(3.00..3.10),
		(3.11..3.24),
		(3.25..3.49),
		(3.50..3.74),
		(3.75..3.89),
		(3.90..3.99),
		(4.00..4.24),
		(4.25..4.49),
		(4.50..4.74),
		(4.75..4.99)]
  end
  def raw_values
  	@roundstratas = [(0.20..0.29),
        (0.30..0.39),
        (0.40..0.49),
        (0.50..0.59),
        (0.60..0.69),
        (0.70..0.74),
        (0.75..0.79),
		(0.80..0.89),
		(0.90..0.99),
		(1.00..1.09),
		(1.10..1.19),
		(1.20..1.24),
		(1.25..1.29),
		(1.30..1.39),
		(1.40..1.49),
		(1.50..1.59),
		(1.60..1.69),
		(1.70..1.74),
		(1.75..1.89),
		(1.90..1.99),
		(2.00..2.10),
		(2.11..2.24),
		(2.25..2.49),
		(2.50..2.74),
		(2.75..2.89),
		(2.90..2.99),
		(3.00..3.10),
		(3.11..3.24),
		(3.25..3.49),
		(3.50..3.74),
		(3.75..3.89),
		(3.90..3.99),
		(4.00..4.24),
		(4.25..4.49),
		(4.50..4.74),
		(4.75..4.99)]
  end
end