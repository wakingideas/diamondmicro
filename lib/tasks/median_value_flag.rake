task :median_value_flag => :environment do

colors = ["D","E","F","G","H","I","J"]
	clarity = ["IF","VVS1","VVS2","VS1","VS2","SI1","SI2"]
	caratsss = ["0.24","0.30","0.40","0.50","0.60","0.70","0.75","0.80","0.90","1.00","1.10","1.20","1.25","1.30","1.40","1.50","1.60","1.70","1.8","1.9","2.00","2.00","2.11","2.25","2.50","2.75","2.90","3.00","3.11","3.25","3.50","3.75","3.90","4.00","4.25","4.50","4.75","4.99"]

    roundstratas = [(0.20..0.29),
        (0.30..0.39),
        (0.40..0.49),
        (0.50..0.59),
        (0.60..0.69),
        (0.70..0.74),
        (0.75..0.79),
		(0.80..0.89),
		(0.90..0.99),
		(1.00..1.09),
		(1.10..1.19),
		(1.20..1.24),
		(1.25..1.29),
		(1.30..1.39),
		(1.40..1.49),
		(1.50..1.59),
		(1.60..1.69),
		(1.70..1.74),
		(1.75..1.89),
		(1.90..1.99),
		(2.00..2.10),
		(2.11..2.24),
		(2.25..2.49),
		(2.50..2.74),
		(2.75..2.89),
		(2.90..2.99),
		(3.00..3.10),
		(3.11..3.24),
		(3.25..3.49),
		(3.50..3.74),
		(3.75..3.89),
		(3.90..3.99),
		(4.00..4.24),
		(4.25..4.49),
		(4.50..4.74),
		(4.75..4.99)]
		
		
	colors.each do |col|
	  clarity.each do |clar|
		roundstratas.each do |rs|
			    caratset = [] 
				for x in rs.step(0.01) do	
					caratset << [RawValue.where(:outlier => false, :color => col ,:clarity => clar, :carat => x).count, x]				
				end
				if caratset.blank?
					for x in rs.step(0.01) do	
					caratset << [RawValue.where(:color => col ,:clarity => clar, :carat => x).count, x]				
				    end
				end    
				carat = ""
			    carat = caratset.max[1]
			   	values = []
				if RawValue.where(:outlier => false, :color => col, :clarity => clar, :carat => carat)
					RawValue.where(:outlier => false, :color => col, :clarity => clar, :carat => carat).each do |rv|
					values << rv.price.to_f
					end
				median = find_median(values)
				unless median.blank?
				RawValue.where(:price => median, :color => col, :clarity => clar, :carat => carat).first.update_attributes(:median => true)
				puts "#{col} - #{clar} - #{carat} - #{median}"
				end
				   
				end
		end

			
	  end

	end
end

def find_median(array)
  middle = array.size/2
  sorted = array.sort_by{ |a| a }
  array.size.odd? ? sorted[middle] : sorted[middle-1]
end