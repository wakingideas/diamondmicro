task :raw_value_gather => :environment do
    require 'open-uri'
	require 'json'

	sidx = 0
	colors = ["D","E","F","G","H","I","J"]
	clarity = ["IF","VVS1","VVS2","VS1","VS2","SI1","SI2"]
	minmax = [[0.20,0.29],
        [0.30,0.39],
        [0.40,0.49],
        [0.50,0.59],
        [0.60,0.69],
        [0.70,0.74],
        [0.75,0.79],
		[0.80,0.89],
		[0.90,0.99],
		[1.00,1.09],
		[1.10,1.19],
		[1.20,1.24],
		[1.25,1.29],
		[1.30,1.39],
		[1.40,1.49],
		[1.50,1.59],
		[1.60,1.69],
		[1.70,1.74],
		[1.75,1.89],
		[1.90,1.99],
		[2.00,2.10],
		[2.11,2.24],
		[2.25,2.49],
		[2.50,2.74],
		[2.75,2.89],
		[2.90,2.99],
		[3.00,3.10],
		[3.11,3.24],
		[3.25,3.49],
		[3.50,3.74],
		[3.75,3.89],
		[3.90,3.99],
		[4.00,4.24],
		[4.25,4.49],
		[4.50,4.74],
		[4.75,4.99]]
	bnd = {}
	colors.each do |col|
	  clarity.each do |clar|
	  minmax.each do |min,max| 
		bnd = JSON.load(open("http://www.bluenile.com/api/public/diamond-search-grid/solr?country=USA&language=en-us&currency=USD&startIndex=0&pageSize=999&shape=RD&sortColumn=price&sortDirection=asc&minFluorescence=None&maxFluorescence=None&minColor=#{col}&maxColor=#{col}&minClarity=#{clar}&maxClarity=#{clar}&minSymmetry=EX&maxSymmetry=EX&minCut=Ideal&maxCut=Ideal&minPolish=EX&maxPolish=EX&minCarat=#{min}&maxCarat=#{max}&_=1444776639226"))
			
			   unless bnd["results"][0].blank?	
				bnd["results"].each do |bnd|
					rv = RawValue.new(
					:sku => bnd["skus"][0],
					:carat => bnd["carat"],
					:shape => bnd["shape"],
					:cut => bnd["cut"],
					:color => bnd["color"],
					:clarity => bnd["clarity"],
					:length_width => bnd["lxwRatio"],
					:depth => bnd["depth"],
					:table => bnd["table"],
					:polish => bnd["polish"],
					:symmetry => bnd["symmetry"],
					:culet => bnd["culet"],
					:fluorescence => bnd["fluorescence"],
					:price => bnd["price"].gsub(/[^\d\.]/, '').to_f )
					rv.save
					rv.maxdiffupdate
					puts "recording value #{bnd["carat"]} - #{bnd["color"]} - #{bnd["clarity"]}"
				 end
			   end
		end
	  end 
	end 
end